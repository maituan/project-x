var express = require('express');
var app = express();
var path = require('path');

const PORT = process.env.PORT || 3001;

console.log('PORT', PORT);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

app.use('/',express.static(__dirname+'/build'));

// app.get(‘/*‘, function (req, res) {
// res.sendFile(path.join(__dirname+‘/build,‘index.html’))
// });

app.listen(PORT, function () {
  console.log(`Example listening on port ${PORT}!`);
});

module.exports = app;
