import React, {Suspense } from 'react';
import logo from './logo.svg';
import './css/App.css';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from 'react-apollo';

import RouteConfig from './config/RootConfig'

const client = new ApolloClient({
  // By default, this client will send queries to the
  //  `/graphql` endpoint on the same host
  // Pass the configuration option { uri: YOUR_GRAPHQL_API_URL } to the `HttpLink` to connect
  // to a different host
  link: new HttpLink(),
  cache: new InMemoryCache(),
});

// loading component for suspence fallback
const Loader = () => (
  <div className="App">
    <img src={logo} className="App-logo" alt="logo" />
    <div>loading...</div>
  </div>
);

const App = () => (
  <ApolloProvider client={client}>
      <Suspense fallback={<Loader />}>
        <RouteConfig />
      </Suspense>
  </ApolloProvider>
);




export default App;
