import 'jest-enzyme';
import sinon from 'sinon';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {
  shallowWithStore,
  shallowWithState,
  mountWithStore,
  mountWithState,
} from 'enzyme-redux';
import { createMockStore } from 'redux-test-utils';

configure({ adapter: new Adapter() });
global.sinon = sinon;
global.shallowWithStore = shallowWithStore;
global.shallowWithState = shallowWithState;
global.mountWithStore = mountWithStore;
global.mountWithState = mountWithState;
global.createMockStore = createMockStore;
