import React from 'react'
import { withTranslation } from 'react-i18next';
class Test extends React.Component{
    
    render(){
        const {t} = this.props
        return(
            <div>{t('description.part2')}</div>
        )
    }
}

export default withTranslation()(Test)
