import React from 'react';
import { useTranslation } from 'react-i18next';
// import { Link } from 'react-router-dom';

// const PUBLIC_URL = process.env.PUBLIC_URL;


function HeaderChangeLang() {
    const { t, i18n } = useTranslation();

    const changeLanguage = lng => {
        i18n.changeLanguage(lng);
    };
    return (
        <div className="App-header">
            <div>{t('title')}</div>
            <button onClick={() => changeLanguage('en')}>en</button>
            <button onClick={() => changeLanguage('vi')}>vi</button>
        </div>
    )
}

class Header extends React.Component {

    render() {
        return (
            <HeaderChangeLang/>
        )
    }
}

export default Header;