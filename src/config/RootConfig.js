import React from "react";
import { Router, Route } from "react-router-dom";

import Test from '../container/Test'
import Header from '../container/layout/Header'
import Footer from '../container/layout/Footer'

import createBrowserHistory from 'history/createBrowserHistory'
const history = createBrowserHistory()

const PUBLIC_URL = process.env.PUBLIC_URL;


const routes = [
    {
      path: "/",
      component: Test
    },
];

const RouteConfig = () => (
    <div>
        <Router basename={PUBLIC_URL} history={history}>
            <div>
                <Header/>
                <div>
                {routes.map((route, i) =>
                    <RouteWithSubRoutes key={i} {...route} />
                )}
                </div>
                <Footer/>
            </div>
        </Router>
    </div>
);

const RouteWithSubRoutes = (route) => {
    return (
        <Route
            exact
            path={route.path}
            render={props => (
                // pass the sub-routes down to keep nesting
                <div>
                    <route.component {...props} routes={route.routes} />
                </div>
            )}
        />
    )
};

export default RouteConfig